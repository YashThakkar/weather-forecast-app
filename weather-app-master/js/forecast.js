
//https://api.openweathermap.org/data/2.5/forecast?q=Mumbai&units=metric&appid=67bdca1e5b6c3ab638dfc05a780960fd


const key = "67bdca1e5b6c3ab638dfc05a780960fd";
const getForecast = async(city) => {
    const base = "https://api.openweathermap.org/data/2.5/forecast"
    const query = `?q=${city}&units=metric&appid=${key}`;
    
    const response = await fetch(base+query);
    //console.log(response);
    if(!response.ok)
            throw new Error("Status Code: "+response.status);
    
    const data = await response.json();
    //console.log(data);
    return data;
}

//getForecast('Gujarat')
//.then(data=>console.log(data))
//.catch(err=>console.warn(err));

